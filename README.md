# Tutorial Docker

## Pre-requisitos

## Instalar Docker y Docker compose

### Linux 

Docker:
- Ubuntu  ->  [enlace](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-es) 
- Otras distribuciones -> [enlace](https://docs.docker.com/engine/install/)

Docker-compose:

- Ubuntu -> [enlace](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-es)
- Otras distribuciones -> [enlace](https://docs.docker.com/compose/install/)

### Windows:

Instalar [Docker-Desktop](https://www.docker.com/products/docker-desktop) . El paquete de instalacion incluye docker y docker-compose.

---------------------------------------------------------------
## Opcional - Modificar red virtual de Docker

Por defecto docker utiliza la red 172.17.0.xx, tanto para la interfaz virtual docker0 como para las redes virtuales por donde se comunicaran los contenedores. 

En caso de que en nuestro lugar de trabajo se utilicen ips en ese rango de red, lo recomendable es modificar el archivo daemon.json que es donde se definen parametros de configuracion de docker, entre ellos los de red.

El archivo daemon.json se encuentra en /etc/docker/daemon.json para sistemas linux o en C:\ProgramData\docker\config\daemon.json para Windows.

Si el archivo no existe, lo creamos y agregamos lo siguiente:

```json
{
 "bip": "10.20.10.5/24",
 "default-address-pools": [
  {
    "base": "10.30.0.0/16",
    "size": 24
  }
  ]
}
```
Por ultimo reiniciamos el servicio

> sudo systemctl restart docker.service

---------------------------------------------------------------

## Comandos basicos
---------------------------------------------------------------

**Descargar imagen de docker hub:**

> docker pull "nombre_imagen":"tag"

**Si quisieramos descargar una imagen de nginx:**

> docker pull nginx:latest

**El tag latest indica que queremos descargar la imagen en su ultima version. En caso de querer utilizar una version en especifico lo hacemos de la siguiente manera:**

> docker pull nginx:1.21.1


**Consultar las imagenes almacenadas localmente**

> docker images

**Comando para iniciar el contenedor con la imagen de nginx:**

> docker run --name nginx_prueba -d -p 8080:80 nginx

### detalle de los parametros del comando:

**indicamos el nombre del contenedor:** --name nginx_prueba 

**Ejecutamos el contenedor como un proceso(daemon)**: -d

**Indicamos los puertos**: -p 8080:80     ("puerto_expuesto":"puerto_interno_contenedor") 

**Imagen con la que sera creado el contenedor**: nginx


**Comando para listar contenedores**

***activos***
> docker ps )

***activos e inactivos***
> docker ps -a   

**Como eliminar un contenedor**

**1) Pausar el contenedor y luego eliminarlo**

> docker stop <nombre_del_contenedor o ID>

> docker rm <nombre_del_contenedor o ID>

**2) Forzar la eliminacion**

> docker rm -f <nombre_del_contenedor o ID>


**Ver logs del contenedor**

> docker logs -f <nombre_del_contenedor o ID>

**ejecutar comandos dentro del contenedor**

> docker exec -it <nombre_del_contenedor> bash

**Comandos de red**

***crear red***
>  docker network create <nombre_de_red>

***listar redes***
>  docker network ls

***inspeccionar red***
>  docker network inspect <nombre_de_red o ID>

**Comandos volumenes**

***crear volumen***
>  docker volume create <nombre_de_volumen>

***listar volumenes***
>  docker volume ls 

***inspeccionar volumen***
>  docker volume inspect <nombre_de_volumen o ID>

## Crear imagen mediante un Dockerfile 

### ¿Que es un Dockerfile?
Un Dockerfile es un archivo de texto plano que contiene una serie de instrucciones necesarias para crear una imagen, que luego utilizaremos para crear nuestro contenedor.

### Ejemplo
Para el ejemplo vamos a utilizar un Dockerfile que contiene las instrucciones necesarias para levantar una aplicacion web basica.

El Dockerfile es el siguiente:

```docker
#Imagen base que vamos a utilizar
FROM php:7.4-apache

#Copiamos la configuracion del virtual host de apache
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

#Habilitamos los modulos de apache
RUN a2enmod rewrite

#Copiamos el codigo Fuente de la aplicacion
COPY ./app /var/www/

#Dar permisos al usuario www-data
RUN chown -R www-data:www-data /var/www

#ejecutamos apache
CMD apachectl -D FOREGROUND 
```

### Construir la imagen

>  docker build -t <nombre_imagen> .

Lo que para el ejemplo seria:

>  docker build -t imagen-app .


### Crear un contenedor con la imagen construida

>  docker run -d --name <nombre_contenedor> <imagen>

Lo que para el ejemplo seria:

>  docker run -d -p 8080:80 --name contenedor-app imagen-app


## Crear contenedores con Docker Compose
Docker Compose es una herramienta que nos permite definir parametros de configuracion para nuestros contenedores y ejecutarlos mediante un solo archivo YAML.

El archivo YAML que utilizaremos para el ejemplo, es el siguiente:

```yaml
version: '3.8'
 
services:
    web:
        build:
            context: ./   #Directorio donde se encuentra el Dockerfile
            dockerfile: Dockerfile
        image: "app-imagen" #Nombre de la imagen
        container_name: "app-contenedor" #Nombre del contenedor
        ports:
            - "8040:80"    # Puertos <puerto_expuesto>:<puerto_interno>
        volumes:
            - ./app:/var/www  # Volumen <directorio_externo>:<directorio_interno>
        networks:
            - desarrollo    # Nombre de la red

networks:
    desarrollo:
        external: true  # Indicamos que la red se creo de forma externa
```

### Crear la red desarrollo
En el archivo YAML tenemos definida una red externa, por lo cual debemos crearla antes de iniciar los contenedores.

> docker network create desarrollo

### Construir la imagen

> docker-compose build

### Crear el contenedor

> docker-compose up

### Parar el contenedor

> docker-compose stop

### Eliminar el contenedor

> docker-compose down